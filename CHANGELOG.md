# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## [Unreleased]

## [1.20] - 2025-01-19
### Changed
- Some performance changes from version 1.18 have been reverted as they could
  lead to plugin crashes in some situations

### Fixed
- Fix plugin crashing when the list of campaigns is refreshed in the browser
  window (see issue #29)
- Fix plugin retaining previous map data while the next map is loading, causing
  dependency plugins to receive incorrect data (see issue #30)

## [1.19] - 2024-11-04
### Added
- Browser window support for the
  [Royal Training Maps campaigns](https://champion-medals.com/campaign/royal)
- Browser window now shows PB delta to Champion medal time

### Changed
- Browser window now shows campaign categories as tabs to align with
  [the website](https://champion-medals.com)

### Fixed
- Champion medals no longer draw on top of the Prestige skin layer

## [1.18] - 2024-10-08
### Changed
- A lot of internal changes, mostly to increase plugin performance! Champion
  Medals is now **faster than ever before**—approximately 1.5–2.5× as fast!
  (thanks to [chips](https://openplanet.dev/u/chipsTM) for suggesting part of
  the changes made in this version!)
- The settings page for the plugin now shows a medal window preview

### Fixed
- Reset animation state when the map is changed (see issue #28)
- Fix medal being visible in the top left without a Trackmania UI element below
  it in live game modes (see issue #13)
- Fix medal not being visible in the top left in solo game modes
- Fix medal not being visible in the pause menu at the end of a round in live
  game modes (see issue #14)
- Plugin will not try to get Champion medal information when in the editor

## [1.17] - 2024-03-16
### Fixed
- Champion medals no longer draw on hidden maps in the campaign Arcade room
  (see issue #24)

## [1.16] - 2024-01-06
### Fixed
- Fix campaign layer visibility when the editor is open (see issue #26)
- Correctly null official campaign live layer when not needed

## [1.15] - 2023-11-21
### Fixed
- Support newest Trackmania update (see issue #25)

## [1.14] - 2023-09-22
### Added
- Browser window: the plugin now has a built-in browser window that shows all
  available Champion medals, similar to
  [the website](https://champion-medals.com). Open the browser window from the
  "Plugins" tab in the menu bar!
  - See Champion medal times and counts
  - See your own personal best times and whether they are faster than the
    Champion medal
  - See how many Champion medals you have in a campaign
  - Open the Champion Medals website directly from the browser window
  - Launch a map directly from the browser window (requires Standard or Club
    access)
- Champion medals now show up in the local play mode
- Champion medals now show up in the official campaign arcade room overview

### Changed
- Minor rename of some settings
- Cleaned up some code

## [1.12] - 2023-06-18
### Fixed
- Resize Champion medal icon in medal window to match the Openplanet UI scale
  (see issue #23)

## [1.11] - 2023-03-30
### Fixed
- Hide Champion medal when the new report menu is opened from the pause menu
  (see issue #22)

## [1.10] - 2023-02-26
### Added
- Option to hide the medal window when the game interface is hidden
  (see issue #21)
- Provider for the [Ticker plugin](https://openplanet.dev/plugin/ticker): if
  the Ticker plugin is installed in addition to Champion Medals, a ticker item
  will be shown for new Champion medals on seasonal campaigns and TOTDs.

### Changed
- Minor rename of some settings

## [1.9] - 2023-01-18
### Fixed
- Champion medals will show up again in campaign menus with the newest
  Trackmania update (see issue #20)

## [1.8] - 2023-01-07
### Fixed
- Fix animation when the Champion medal has already been granted (see issue
  #19)
- Fix potential game crash when it is closed, preventing a clean shutdown (see
  issue #18)

## [1.7] - 2022-11-21
### Fixed
- Fix animation when the first PB grants the Champion medal (see issue #15)
- Fix Champion medal alignment in club campaigns (see issue #16)
- Fix club rooms (see issue #17)

### Changed
- Minor improvements to debugging tools

## [1.6] - 2022-09-08
### Fixed
- Fix support for redesigned monthly campaign layer (see issue #9)
- Hide Champion medal when the new settings menu is opened from the solo pause
  menu (see issue #9)
- Prevent a possible plugin crash when the game's menu structure changes (see
  issue #10)
- Small code quality improvements (see issues #11 and #12)

## [1.5] - 2022-07-20
### Added
- Exported function that other Openplanet plugins can depend on (see issue #8).
  See [the dependency example plugin](https://gitlab.com/naninf/champion-medals-dependency)
  for an example of how to use this function.
- Debug window to aid in future plugin development and maintenance

## [1.4] - 2022-07-17
### Added
- Animate the Champion medal when the player first obtains it

### Fixed
- Medals are now hidden when the close game dialog is visible (see issue #6)
- Audio effect now only plays during the first PB below the Champion medal time
  (see issue #7)

## [1.3] - 2022-07-13
### Fixed
- Medals are now hidden in live matches when the player hides the game
  interface (see issue #5)
- Medals are now hidden when a viewport overlay (e.g. settings) is visible (see
  issue #1)

## [1.2] - 2022-07-09
### Fixed
- The plugin will no longer blow your ears out if you're not playing Trackmania
  at max volume (see issue #4)

### Changed
- An equal time will show up as −0 in the delta column instead of +0

## [1.1] - 2022-07-09
### Added
- Play a sound when the player obtains the Champion medal (see issue #2)

### Fixed
- Prevent the plugin from spamming requests in the map editor (see issue #3)

## [1.0] - 2022-07-07
### Added
- Initial release

[Unreleased]: https://gitlab.com/naninf/champion-medals/-/compare/1.20...main
[1.20]: https://gitlab.com/naninf/champion-medals/-/compare/1.19...1.20
[1.19]: https://gitlab.com/naninf/champion-medals/-/compare/1.18...1.19
[1.18]: https://gitlab.com/naninf/champion-medals/-/compare/1.17...1.18
[1.17]: https://gitlab.com/naninf/champion-medals/-/compare/1.16...1.17
[1.16]: https://gitlab.com/naninf/champion-medals/-/compare/1.15...1.16
[1.15]: https://gitlab.com/naninf/champion-medals/-/compare/1.14...1.15
[1.14]: https://gitlab.com/naninf/champion-medals/-/compare/1.12...1.14
[1.12]: https://gitlab.com/naninf/champion-medals/-/compare/1.11...1.12
[1.11]: https://gitlab.com/naninf/champion-medals/-/compare/1.10...1.11
[1.10]: https://gitlab.com/naninf/champion-medals/-/compare/1.9...1.10
[1.9]: https://gitlab.com/naninf/champion-medals/-/compare/1.8...1.9
[1.8]: https://gitlab.com/naninf/champion-medals/-/compare/1.7...1.8
[1.7]: https://gitlab.com/naninf/champion-medals/-/compare/1.6...1.7
[1.6]: https://gitlab.com/naninf/champion-medals/-/compare/1.5...1.6
[1.5]: https://gitlab.com/naninf/champion-medals/-/compare/1.4...1.5
[1.4]: https://gitlab.com/naninf/champion-medals/-/compare/1.3...1.4
[1.3]: https://gitlab.com/naninf/champion-medals/-/compare/1.2...1.3
[1.2]: https://gitlab.com/naninf/champion-medals/-/compare/1.1...1.2
[1.1]: https://gitlab.com/naninf/champion-medals/-/compare/1.0...1.1
[1.0]: https://gitlab.com/naninf/champion-medals/-/tags/1.0
