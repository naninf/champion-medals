# Champion Medals

**Are author medals no longer a challenge for you? Are you looking for
something more difficult? Look no further than Champion Medals, which gives you
a fifth medal to race for!**

**_[Website](https://champion-medals.com)_**

Champion Medals adds an analogue to Trackmania Turbo's Super Trackmaster medals
to Trackmania 2020. The pink Champion medal introduced by this plugin aims to
be a medal more difficult to achieve than the Author Medal, without being
impossible. Champion medals are currently available for all campaigns starting
from Spring 2022, and all TOTDs starting from May 1st. New Champion medals are
added 7 days after the release of a new seasonal campaign, and 1 hour after the
release of a new TOTD. Champion medals are automatically calculated based on
the map's leaderboard at that time.

When you obtain a Champion medal, it will be visible in-game (almost)
everywhere the other medals are also visible. Furthermore, this plugin comes
with a built-in window to compare your personal best to the Champion medal time
(similar to the Ultimate Medals plugin). Additionally, all available Champion
medal times can be viewed on the website linked above.

## Contact

The Champion Medals plugin and associated website is developed by NaNInf.

For any updates regarding the Champion Medals plugin, follow me on Twitter:
[@NaNInf_](https://twitter.com/NaNInf_).

If you have any questions, issues, or suggestions regarding the Champion Medals
plugin, you can contact me via Discord: NaNInf#6265.

If you want to use the Champion Medals dataset yourself for an interesting
Trackmania integration, please contact me via Discord: NaNInf#6265. Do not
scrape the website or the endpoints used by the Openplanet plugin without prior
permission.

## Privacy policy

View the privacy policy on
[the Champion Medals website.](https://champion-medals.com/privacy-policy)

## License

The Champion Medals source code is available under
[the GPLv3 license.](LICENSE.md)
