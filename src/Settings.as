/**
 * Champion Medals
 * Copyright (c) 2023 NaNInf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

void draw_medal_window_demo() {
    if (setting_window_enable) {
        UI::Text("Window preview");
        medal_window.Draw(true);
    }
}

[Setting category="Interface" name="Enable medal window" description="Show the Champion medal time in a small window" afterrender="draw_medal_window_demo"]
bool setting_window_enable = true;

[Setting category="Interface" name="Show PB" description="Show your PB in the Champion medal window" if="setting_window_enable"]
bool setting_window_show_pb = true;

[Setting category="Interface" name="Show PB delta" description="Show the difference between your PB and the Champion medal" if="setting_window_enable"]
bool setting_window_show_delta = true;

[Setting category="Interface" name="Hide medal window when interface is hidden" description="Hide the Champion medal window when the game interface is hidden" if="setting_window_enable"]
bool setting_window_hide_with_interface = false;

#if DEPENDENCY_TICKER
[Setting category="Interface" name="Show Champion Medals in ticker" description="Show Champion medals for official campaigns and tracks of the day in the Ticker plugin (requires plugin reload)"]
bool setting_ticker_show = true;
#endif

#if SIG_DEVELOPER
[Setting category="Advanced" name="API base URL"]
string setting_api_base_url = "https://champion-medals.com";

[Setting category="Advanced" name="Enable debug log"]
bool setting_debug_log = false;

[Setting category="Advanced" name="Show log messages as notifications"]
bool setting_debug_log_notifications = false;

#else
string setting_api_base_url = "https://champion-medals.com";
bool setting_debug_log = false;
bool setting_debug_log_notifications = false;
#endif

[Setting category="Advanced" name="Enable debug window"]
bool setting_debug_window_enable = false;

[Setting hidden]
bool setting_browser_window_enable = true;
