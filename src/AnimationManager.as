/**
 * Champion Medals
 * Copyright (c) 2023 NaNInf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

class AnimationManager {
    Audio::Sample@ medal_cm;
    bool animation_queued = false;
    bool animation_playing = false;
    uint64 animation_start_time = 0;

    uint64 animation_duration = 200;

    AnimationManager() {
        @medal_cm = Audio::LoadSample("assets/medal_cm.wav");
    }

    void QueueAnimation() {
        animation_queued = true;
    }

    void ResetAnimation() {
        animation_queued = false;
        animation_playing = false;

        Layers@ layers = layer_manager.layers;

        cast<PlaygroundLayer>(layers[LayerIndex::EndMenuLayer]).scale = 1.0;
        cast<PlaygroundLayer>(layers[LayerIndex::EndMenuLayer]).alpha = 1.0;

        cast<PlaygroundLayer>(layers[LayerIndex::LocalEndMenuLayer]).scale = 1.0;
        cast<PlaygroundLayer>(layers[LayerIndex::LocalEndMenuLayer]).alpha = 1.0;
    }

    void Update() {
        Layers@ layers = layer_manager.layers;

        if (animation_queued) {
            // play audio once the end layer or the live new PB layer is visible
            if (layers[LayerIndex::EndMenuLayer].IsVisible() ||
                layers[LayerIndex::LocalEndMenuLayer].IsVisible() ||
                layers[LayerIndex::LiveNewPBLayer].IsVisible()) {
                animation_queued = false;
                Audio::Play(medal_cm, Volume(0.25));

                // only play animation for the end layer
                if (layers[LayerIndex::EndMenuLayer].IsVisible() ||
                    layers[LayerIndex::LocalEndMenuLayer].IsVisible()) {
                    animation_playing = true;
                    animation_start_time = Time::Now;
                }
            }
        }

        if (animation_playing) {
            float t = Math::Clamp((Time::Now - animation_start_time) / float(animation_duration), 0.0, 1.0);

            if (t == 1.0 || !(layers[LayerIndex::EndMenuLayer].IsVisible() ||
                              layers[LayerIndex::LocalEndMenuLayer].IsVisible())) {
                ResetAnimation();
            }

            float scale = -Math::Pow(t, 2) + 2;
            float alpha = -Math::Pow(t - 1, 2) + 1;

            cast<PlaygroundLayer>(layers[LayerIndex::EndMenuLayer]).scale = scale;
            cast<PlaygroundLayer>(layers[LayerIndex::EndMenuLayer]).alpha = alpha;

            cast<PlaygroundLayer>(layers[LayerIndex::LocalEndMenuLayer]).scale = scale;
            cast<PlaygroundLayer>(layers[LayerIndex::LocalEndMenuLayer]).alpha = alpha;
        }
    }

    private float Volume(float base_volume) {
        float game_volume_factor = Math::Exp(GetApp().AudioPort.SoundVolume / 10);
        return base_volume * game_volume_factor;
    }
}
