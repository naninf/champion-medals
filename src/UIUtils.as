/**
 * Champion Medals
 * Copyright (c) 2023 NaNInf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace UIUtils {
    // from Trackmania/Scripts/Libs/Nadeo/MenuLibs/Common/Menu/Components/MedalStack.Script.txt
    const float medal_size = 12;

    // from Trackmania/Scripts/ManiaApps/Nadeo/TMNext/TrackMania/UIModules/RaceMapInfos.Script.txt
    //  and Trackmania/Scripts/Libs/Nadeo/MenuLibs/Common/Menu/Components/MedalStack.Script.txt
    //  and a manual additional scaling factor
    const float playground_medal_scale = 0.8 * 1.7 * 1.2;

    // from Trackmania/Scripts/ManiaApps/Nadeo/TMxSM/Race/UIModules/Record_Client.Script.txt
    const float playground_medal_scale_record = 0.6 * 36.5 / medal_size;

    // manually determined from offsets (left-aligned) with a manual offset
    const vec2 playground_medal_offset = vec2(10.96, 0) + vec2(1.2, 0);

    // REGULAR CAMPAIGN CONSTS
    // AbsolutePosition of frame-map-0-0
    const vec2 regular_top_left = vec2(-126.2056, 0.25);

    // AbsolutePosition of frame-map-0-0 in club room
    const vec2 regular_top_left_club = vec2(-125.765, 2.75);

    // frame-map-1-0 - frame-map-0-0
    const vec2 regular_dx = vec2(36, 0);

    // frame-map-0-1 - frame-map-0-0
    const vec2 regular_dy = vec2(-2.0277, -11.5);

    // author medal quad-medal - frame-map
    const vec2 regular_at_offset = vec2(24.475, -3.99);

    // author medal quad-medal - gold medal quad-medal
    const vec2 regular_medal_offset = vec2(2, 0);

    // AbsoluteScale of quad-medal
    const float regular_medal_scale = 0.8;

    // MONTHLY CAMPAIGN CONSTS
    // AbsolutePosition of frame-map-0-0
    const vec2 monthly_top_left = vec2(-137.756, 0.25);

    // frame-map-1-0 - frame-map-0-0
    const vec2 monthly_dx = vec2(29.1, 0);

    // frame-map-0-1 - frame-map-0-0
    const vec2 monthly_dy = vec2(-2.0277, -11.5);

    // author medal quad-medal - frame-map
    const vec2 monthly_at_offset = vec2(17.72, -3.61);

    // author medal quad-medal - gold medal quad-medal
    const vec2 monthly_medal_offset = vec2(1.9, 0);

    // AbsoluteScale of quad-medal
    const float monthly_medal_scale = 0.76;

    vec2 RegularCampaignMedalCoords(uint index, bool is_club) {
        uint row = index % 5;
        uint column = index / 5;
        vec2 top_left = is_club ? regular_top_left_club : regular_top_left;

        return MenuCoordsToScreenSpace(top_left + regular_dx * column + regular_dy * row + regular_at_offset + regular_medal_offset);
    }

    vec2 MonthlyCampaignMedalCoords(uint index) {
        uint row = index / 7;
        uint column = index % 7;

        return MenuCoordsToScreenSpace(monthly_top_left + monthly_dx * column + monthly_dy * row + monthly_at_offset + monthly_medal_offset);
    }

    vec2 RegularCampaignMedalSize() {
        return MenuSizeToScreenSpace(vec2(medal_size * regular_medal_scale, medal_size * regular_medal_scale));
    }

    vec2 MonthlyCampaignMedalSize() {
        return MenuSizeToScreenSpace(vec2(medal_size * monthly_medal_scale, medal_size * monthly_medal_scale));
    }

    vec2 PlaygroundMedalCoords(vec2 frame_content_coords) {
        return PlaygroundCoordsToScreenSpace(frame_content_coords);
    }

    vec2 PlaygroundMedalCoordsFromStack(vec2 frame_content_coords) {
        return PlaygroundCoordsToScreenSpace(frame_content_coords + playground_medal_offset);
    }

    vec2 PlaygroundMedalSize() {
        return PlaygroundSizeToScreenSpace(vec2(medal_size * playground_medal_scale, medal_size * playground_medal_scale));
    }

    vec2 PlaygroundMedalSizeRecord() {
        return PlaygroundSizeToScreenSpace(vec2(medal_size * playground_medal_scale_record, medal_size * playground_medal_scale_record));
    }

    float MenuScaleUnit() {
        float w = Draw::GetWidth();
        float h = Draw::GetHeight();

        return (w / h < 16.0 / 9.0) ? w / 320 : h / 180;
    }

    vec2 MenuCoordsToScreenSpace(vec2 coords) {
        float w = Draw::GetWidth();
        float h = Draw::GetHeight();

        float unit = MenuScaleUnit();
        vec2 scale = vec2(unit, -unit);

        return vec2(w / 2, h / 2) + coords * scale;
    }

    vec2 MenuSizeToScreenSpace(vec2 size) {
        float unit = MenuScaleUnit();
        return size * unit;
    }

    float PlaygroundScaleUnit() {
        float w = Draw::GetWidth();
        float h = Draw::GetHeight();

        return (w / h > 16.0 / 9.0) ? w / 320 : h / 180;
    }

    vec2 PlaygroundCoordsToScreenSpace(vec2 coords) {
        float w = Draw::GetWidth();
        float h = Draw::GetHeight();

        vec2 scale;
        if (w / h > 16.0 / 9.0) {
            scale = vec2(h / 180.0, -h / 180.0);
        } else {
            scale = vec2(w / 320.0, -h / 180.0);
        }

        return vec2(w / 2, h / 2) + coords * scale;
    }

    vec2 PlaygroundSizeToScreenSpace(vec2 size) {
        float h = Draw::GetHeight();
        float unit = h / 180.0;
        return size * unit;
    }

    string DeltaString(int delta) {
        if (delta <= 0) {
            return "\\$77f\u2212" + Time::Format(-delta);
        } else {
            return "\\$f77+" + Time::Format(delta);
        }
    }
}
