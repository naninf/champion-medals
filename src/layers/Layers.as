/**
 * Champion Medals
 * Copyright (c) 2023 NaNInf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

enum LayerIndex {
    RegularCampaignLayer,
    RegularCampaignLiveLayer,
    RegularOfficialCampaignLiveLayer,
    MonthlyCampaignLayer,
    StartMenuLayer,
    PauseMenuLayer,
    EndMenuLayer,
    LocalEndMenuLayer,
    RecordLayer,
    LivePauseMenuLayer,
    LiveNewPBLayer
};

class Layers {
    // layers which can display a medal
    RegularCampaignLayer@ regular_campaign_layer;
    RegularCampaignLayer@ regular_campaign_live_layer;
    RegularCampaignLayer@ regular_official_campaign_live_layer;
    MonthlyCampaignLayer@ monthly_campaign_layer;

    StartMenuLayer@ start_menu_layer;
    PauseMenuLayer@ pause_menu_layer;
    EndMenuLayer@ end_menu_layer;

    LocalEndMenuLayer@ local_end_menu_layer;
    RecordLayer@ record_layer;

    LivePauseMenuLayer@ live_pause_menu_layer;
    LiveNewPBLayer@ live_new_pb_layer;

    // layers which can block the display of a medal
    // these should not be in `layers`
    PrestigeLayer@ prestige_layer;
    ScoresTableLayer@ scores_table_layer;
    CloseDialogLayer@ close_dialog_layer;

    array<Layer@> layers;

    Layers() {
        @regular_campaign_layer = RegularCampaignLayer();
        @regular_campaign_live_layer = RegularCampaignLayer();
        @regular_official_campaign_live_layer = RegularCampaignLayer(10);
        @monthly_campaign_layer = MonthlyCampaignLayer();
        @start_menu_layer = StartMenuLayer();
        @pause_menu_layer = PauseMenuLayer();
        @end_menu_layer = EndMenuLayer();
        @local_end_menu_layer = LocalEndMenuLayer();
        @record_layer = RecordLayer();
        @live_pause_menu_layer = LivePauseMenuLayer();
        @live_new_pb_layer = LiveNewPBLayer();
        @prestige_layer = PrestigeLayer();
        @scores_table_layer = ScoresTableLayer();
        @close_dialog_layer = CloseDialogLayer();

        layers = {
            regular_campaign_layer,
            regular_campaign_live_layer,
            regular_official_campaign_live_layer,
            monthly_campaign_layer,
            start_menu_layer,
            pause_menu_layer,
            end_menu_layer,
            local_end_menu_layer,
            record_layer,
            live_pause_menu_layer,
            live_new_pb_layer
        };
    }

    Layer@ opIndex(uint index) {
        return layers[index];
    }

    uint get_Length() {
        return layers.Length;
    }
}
