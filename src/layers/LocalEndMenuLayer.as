/**
 * Champion Medals
 * Copyright (c) 2023 NaNInf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

class LocalEndMenuLayer : PlaygroundLayer {
    bool IsVisible() override {
        if (ui_layer is null || ui_layer.LocalPage is null) return false;

        CGameManialinkControl@ frame_global = ui_layer.LocalPage.GetFirstChild("frame-global");
        if (frame_global is null || !frame_global.Visible) return false;

        return true;
    }
}
