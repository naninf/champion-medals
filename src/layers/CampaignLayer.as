/**
 * Champion Medals
 * Copyright (c) 2023 NaNInf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

class CampaignLayer : Layer {
    string identifier;
    string previous_identifier;

    protected Campaign@ campaign;

    MwFastBuffer<CGameUILayer@> GetUILayers() override {
        CTrackMania@ app = cast<CTrackMania@>(GetApp());
        if (app.MenuManager.MenuCustom_CurrentManiaApp !is null) {
            return app.MenuManager.MenuCustom_CurrentManiaApp.UILayers;
        }
        return Layer::GetUILayers();
    }

    bool IsVisible() override {
        CGameCtnApp@ app = GetApp();
        return ui_layer !is null &&
            ui_layer.LocalPage !is null &&
            ui_layer.IsVisible &&
            app.Network.ClientManiaAppPlayground is null &&
            app.Editor is null;
    }

    void CheckUpdate() override {
        if (!IsVisible()) {
            @campaign = null;
            previous_identifier = identifier = "";
            return;
        }
        identifier = GetIdentifier();
        if (identifier != previous_identifier) Update();
        previous_identifier = identifier;
    }

    private void Update() override {
        log("Updating campaign layer, " + identifier, LogLevel::Print);
        @campaign = null;
        @campaign = Api::GetCampaign(identifier);
    }

    private bool ShouldDraw() override {
        if (!IsVisible() ||
            LoadingVisible() ||
            CloseDialogVisible() ||
            campaign is null ||
            !campaign.has_cms)
            return false;

        // check if the region or club tabs are selected
        CGameManialinkControl@ frame_zone_selection = ui_layer.LocalPage.GetFirstChild("frame-zone-selection");
        if (frame_zone_selection !is null && frame_zone_selection.Visible) return false;

        CGameManialinkControl@ frame_pinned_club = ui_layer.LocalPage.GetFirstChild("frame-pinned-club");
        if (frame_pinned_club !is null && frame_pinned_club.Visible) return false;

        return true;
    }
}
