/**
 * Champion Medals
 * Copyright (c) 2023 NaNInf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

class RegularCampaignLayer : CampaignLayer {
    uint max_maps;

    // default max_maps is 2^31 - 1 instead of 2^32 - 1
    // as `Math::Min` operates on ints, not uints
    RegularCampaignLayer(uint _max_maps = 2147483647) {
        max_maps = _max_maps;
    }

    private string GetIdentifier() override {
        string club = GetLabelValue(GetFrame("button-club"), "menu-libs-expendable-button_label-button-text").SubStr(15);
        string campaign = GetLabelValue("label-title");
        return club + "|" + campaign;
    }

    void Draw() override {
        if (!ShouldDraw()) return;

        bool is_club_layer = IsClubLayer();

        for (uint i = 0; i < uint(Math::Min(campaign.maps.Length, max_maps)); ++i) {
            Map@ map = cast<Map@>(campaign.maps[i]);
            if (map.cm_awarded()) {
                vec2 coords = UIUtils::RegularCampaignMedalCoords(map.index, is_club_layer);
                vec2 size = UIUtils::RegularCampaignMedalSize();
                medal.NvgImage(size, coords, Align::Left | Align::Middle);
            }
        }
    }

    private bool IsClubLayer() {
        CGameManialinkFrame@ frame = GetFrame("button-club");
        return frame !is null && frame.Visible;
    }
}
