/**
 * Champion Medals
 * Copyright (c) 2023 NaNInf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

class Layer {
    int ui_layer_index = -1;

    Layer() {}

    CGameUILayer@ get_ui_layer() {
        if (ui_layer_index < 0) return null;
        MwFastBuffer<CGameUILayer@> ui_layers = GetUILayers();
        if (uint(ui_layer_index) >= ui_layers.Length) return null;
        return ui_layers[ui_layer_index];
    }

    MwFastBuffer<CGameUILayer@> GetUILayers() {
        return MwFastBuffer<CGameUILayer@>();
    }

    void SetUILayerIndex(int _ui_layer_index) final {
        if (ui_layer_index >= 0) {
            if (ui_layer_index == _ui_layer_index) {
                log("Setting duplicate UI layer index", LogLevel::Warn);
            } else {
                log("Setting multiple UI layer indices", LogLevel::Error);
            }
        }
        ui_layer_index = _ui_layer_index;
    }

    void ClearUILayerIndex() final {
        ui_layer_index = -1;
    }

    bool UILayerIsNull() final {
        return ui_layer is null;
    }

    bool IsVisible() {
        return ui_layer !is null &&
            ui_layer.LocalPage !is null &&
            ui_layer.IsVisible;
    }

    void CheckUpdate() {}

    void Draw() {
        if (!ShouldDraw()) return;
    }

    string GetIdentifier() {
        return "";
    }

    bool ShouldDraw() {
        return true;
    }

    protected CGameManialinkFrame@ GetFrame(const string &in control_id) final {
        return (ui_layer !is null && ui_layer.LocalPage !is null) ?
            cast<CGameManialinkFrame@>(ui_layer.LocalPage.GetFirstChild(control_id)) : null;
    }

    protected string GetLabelValue(const string &in control_id) final {
        if (ui_layer is null || ui_layer.LocalPage is null) return "";
        CGameManialinkLabel@ label = cast<CGameManialinkLabel@>(ui_layer.LocalPage.GetFirstChild(control_id));
        if (label is null) return "";
        return label.Value;
    }

    protected string GetLabelValue(CGameManialinkFrame@ frame, const string &in control_id) final {
        if (frame is null) return "";
        CGameManialinkLabel@ label = cast<CGameManialinkLabel@>(frame.GetFirstChild(control_id));
        if (label is null) return "";
        return label.Value;
    }

    protected void Update() {}

    protected bool LoadingVisible() final {
        auto load_progress = GetApp().LoadProgress;
        return load_progress !is null && load_progress.State != NGameLoadProgress::EState::Disabled;
    }

    protected bool CloseDialogVisible() final {
        return layer_manager.layers.close_dialog_layer !is null && layer_manager.layers.close_dialog_layer.IsVisible();
    }
}
