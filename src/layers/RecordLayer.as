/**
 * Champion Medals
 * Copyright (c) 2023 NaNInf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

class RecordLayer : PlaygroundLayer {
    bool IsVisible() override {
        if (ui_layer is null || ui_layer.LocalPage is null) return false;

        CGameManialinkFrame@ main_frame = ui_layer.LocalPage.MainFrame;
        if (main_frame is null || !main_frame.Visible) return false;

        CGameManialinkControl@ frame_global = ui_layer.LocalPage.GetFirstChild("frame-global");
        if (frame_global is null || !frame_global.Visible) return false;

        CGameManialinkControl@ frame_medal = ui_layer.LocalPage.GetFirstChild("frame-medal");
        if (frame_medal is null || !frame_medal.Visible) return false;

        return true;
    }

    void Draw() override {
        if (!ShouldDraw()) return;

        CGameManialinkControl@ medal_quad = ui_layer.LocalPage.GetFirstChild("quad-medal");
        if (medal_quad is null) return;
        vec2 medal_quad_pos = medal_quad.AbsolutePosition_V3;

        vec2 coords = UIUtils::PlaygroundMedalCoords(medal_quad_pos);
        vec2 size = UIUtils::PlaygroundMedalSizeRecord();
        medal.NvgImage(size, coords, Align::Center | Align::Middle);
    }
}
