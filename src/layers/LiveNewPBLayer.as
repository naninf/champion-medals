/**
 * Champion Medals
 * Copyright (c) 2023 NaNInf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

class LiveNewPBLayer : PlaygroundLayer {
    bool IsVisible() override {
        if (ui_layer is null || ui_layer.LocalPage is null) return false;

        CGameManialinkFrame@ main_frame = ui_layer.LocalPage.MainFrame;
        if (main_frame is null || !main_frame.Visible) return false;

        CGameManialinkControl@ frame_celebration_time = ui_layer.LocalPage.GetFirstChild("frame-celebration-time");
        if (frame_celebration_time is null || !frame_celebration_time.Visible) return false;

        return true;
    }

    void Draw() override {}
}
