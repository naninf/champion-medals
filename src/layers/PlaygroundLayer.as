/**
 * Champion Medals
 * Copyright (c) 2023 NaNInf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

class PlaygroundLayer : Layer {
    float scale = 1.0;
    float alpha = 1.0;

    MwFastBuffer<CGameUILayer@> GetUILayers() override {
        CTrackMania@ app = cast<CTrackMania@>(GetApp());
        if (app.Network.ClientManiaAppPlayground !is null) {
            return app.Network.ClientManiaAppPlayground.UILayers;
        }
        return Layer::GetUILayers();
    }

    // updates happen through UpdatePlaygroundMap
    void CheckUpdate() final override {}

    void Draw() override {
        if (!ShouldDraw()) return;

        CGameManialinkControl@ medal_stack = ui_layer.LocalPage.GetFirstChild("ComponentMedalStack_frame-global");
        if (medal_stack is null) return;
        vec2 medal_stack_pos = medal_stack.AbsolutePosition_V3;

        vec2 coords = UIUtils::PlaygroundMedalCoordsFromStack(medal_stack_pos);
        vec2 size = UIUtils::PlaygroundMedalSize();
        medal.NvgImage(size, coords, Align::Left | Align::Middle, scale, alpha);
    }

    private bool ShouldDraw() override {
        return IsVisible() &&
            !LoadingVisible() &&
            !CloseDialogVisible() &&
            playground_map !is null &&
            playground_map.cm_awarded();
    }

    private void Update() final override {}
}
