/**
 * Champion Medals
 * Copyright (c) 2023 NaNInf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

class MonthlyCampaignLayer : CampaignLayer {
    private string GetIdentifier() override {
        return GetLabelValue("label-title");
    }

    void Draw() override {
        if (!ShouldDraw()) return;

        for (uint i = 0; i < campaign.maps.Length; ++i) {
            Map@ map = cast<Map@>(campaign.maps[i]);
            if (map.cm_awarded()) {
                vec2 coords = UIUtils::MonthlyCampaignMedalCoords(map.index);
                vec2 size = UIUtils::MonthlyCampaignMedalSize();
                medal.NvgImage(size, coords, Align::Left | Align::Middle);
            }
        }
    }
}
