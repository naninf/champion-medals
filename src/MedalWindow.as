/**
 * Champion Medals
 * Copyright (c) 2023 NaNInf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

class MedalWindow {
    MedalWindow() {}

    void Draw(bool demo = false) {
        if (!demo &&
            (!setting_window_enable ||
             playground_map is null ||
             !playground_map.has_cm ||
             (setting_window_hide_with_interface && !UI::IsGameUIVisible())))
            return;

        int window_flags = UI::WindowFlags::NoTitleBar |
                           UI::WindowFlags::NoResize |
                           UI::WindowFlags::NoScrollbar |
                           UI::WindowFlags::NoScrollWithMouse |
                           UI::WindowFlags::AlwaysAutoResize |
                           UI::WindowFlags::NoDocking;

        if (!UI::IsOverlayShown()) window_flags |= UI::WindowFlags::NoMove;

        bool cm_awarded;
        uint cm_time, pb_time;

        if (!demo) {
            cm_time = playground_map.cm_time;
            pb_time = playground_map.pb_time;
            cm_awarded = playground_map.cm_awarded();

            UI::Begin("Champion Medals", window_flags);
        } else {
            cm_time = 31415;
            pb_time = 31337;
            cm_awarded = pb_time <= cm_time;

            int child_flags = UI::ChildFlags::Border |
                              UI::ChildFlags::AutoResizeX |
                              UI::ChildFlags::AutoResizeY |
                              UI::ChildFlags::AlwaysAutoResize |
                              UI::ChildFlags::FrameStyle;

            window_flags &= ~UI::WindowFlags::AlwaysAutoResize;

            UI::BeginChild("Champion Medals", vec2(), child_flags, window_flags);
        }

        // only show PB/delta when those settings are checked and a PB is available
        bool pb_available = pb_time != uint(-1) && pb_time != uint(-1) - 1;
        bool show_pb = setting_window_show_pb && pb_available;
        bool show_pb_delta = setting_window_show_delta && pb_available;

        int table_columns = 3 + (show_pb_delta ? 1 : 0);

        if (UI::BeginTable("##main", table_columns)) {
            if (show_pb && cm_awarded) {
                TableRowPB(cm_time, pb_time, show_pb_delta);
            }
            TableRowChampion(cm_time, pb_time, show_pb_delta && !show_pb);
            if (show_pb && !cm_awarded) {
                TableRowPB(cm_time, pb_time, show_pb_delta);
            }

            UI::EndTable();
        }

        if (!demo) {
            UI::End();
        } else {
            UI::EndChild();
        }
    }

    void TableRowChampion(uint cm_time, uint pb_time, bool show_pb_delta) {
        UI::TableNextRow();

        UI::TableNextColumn();
        medal.WindowImage(UI::GetScale() * vec2(16, 16));

        UI::TableNextColumn();
        UI::Text("Champion");

        UI::TableNextColumn();
        UI::Text(Time::Format(cm_time));

        if (show_pb_delta) TableColumnDelta(cm_time, pb_time);
    }

    void TableRowPB(uint cm_time, uint pb_time, bool show_pb_delta) {
        UI::TableNextRow();

        UI::TableNextColumn();
        UI::TableNextColumn();
        UI::Text("Pers. Best");

        UI::TableNextColumn();
        UI::Text(Time::Format(pb_time));

        if (show_pb_delta) TableColumnDelta(cm_time, pb_time);
    }

    void TableColumnDelta(uint cm_time, uint pb_time) {
        UI::TableNextColumn();
        int delta = int(pb_time) - int(cm_time);
        UI::Text(UIUtils::DeltaString(delta));
    }
}
