/**
 * Champion Medals
 * Copyright (c) 2023 NaNInf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

LayerManager@ layer_manager;
MedalWindow@ medal_window;
BrowserWindow@ browser_window;
DebugWindow@ debug_window;
Medal@ medal;
Map@ playground_map;
AnimationManager@ animation_manager;
FontManager@ font_manager;

int64 frame_count = 0;

void Main() {
    @layer_manager = LayerManager();
    @medal_window = MedalWindow();
    @browser_window = BrowserWindow();
    @debug_window = DebugWindow();
    @animation_manager = AnimationManager();
    @font_manager = FontManager();

    NadeoServices::AddAudience("NadeoLiveServices");
    while (!NadeoServices::IsAuthenticated("NadeoLiveServices")) yield();

    @medal = Medal();

    RegisterTicker();

    while (true) {
        layer_manager.CheckUpdate();
        browser_window.CheckUpdate();

        UpdatePlaygroundMap();

        yield();
    }
}

void Render() {
    layer_manager.CheckUpdateUILayers();

    animation_manager.Update();

    layer_manager.Draw();
    medal_window.Draw();

    ++frame_count;
}

void RenderInterface() {
    browser_window.Draw();
    debug_window.Draw();
}

void RenderMenu() {
    browser_window.RenderMenu();
}

void UpdatePlaygroundMap() {
    CTrackMania@ app = cast<CTrackMania@>(GetApp());

    if (app.Network.ClientManiaAppPlayground is null) {
        if (@playground_map !is null) @playground_map = null;
        animation_manager.ResetAnimation();
        return;
    }
    if (app.RootMap !is null &&
        app.RootMap.MapInfo !is null &&
        app.Editor is null) {
        string uid = app.RootMap.MapInfo.MapUid;
        if (playground_map is null || playground_map.uid != uid) {
            animation_manager.ResetAnimation();
            @playground_map = null;
            @playground_map = Api::GetMapChampionMedal(uid);
        }
    }
    if (playground_map !is null && app.Network.ClientManiaAppPlayground !is null) {
        auto user_manager = app.Network.ClientManiaAppPlayground.UserMgr;
        MwId user_id;
        if (user_manager.Users.Length > 0) {
            user_id = user_manager.Users[0].Id;
        } else {
            user_id.Value = uint(-1);
        }

        auto score_manager = app.Network.ClientManiaAppPlayground.ScoreMgr;
        uint pb_time = score_manager.Map_GetRecord_v2(user_id, playground_map.uid, "PersonalBest", "", "TimeAttack", "");
        uint prev_pb_time = playground_map.pb_time;
        playground_map.pb_time = pb_time;

        if (prev_pb_time != uint(-1) - 1 && prev_pb_time != pb_time &&
            !playground_map.cm_awarded(prev_pb_time) && playground_map.cm_awarded()) {
            log("Awarded CM!", LogLevel::Trace);
            animation_manager.QueueAnimation();
        }
    }
}
