/**
 * Champion Medals
 * Copyright (c) 2023 NaNInf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#if DEPENDENCY_TICKER
void RegisterTicker() {
    if (setting_ticker_show) Ticker::registerTickerItemProviderAddon(CMTickerItemProvider());
}

class CMTickerItemProvider : Ticker::TickerItemProvider {
    array<Ticker::TickerItem@> items;

    string getID() {
        return "ChampionMedals/Ticker";
    }

    void OnUpdate() {
        items = Api::GetTicker();
    }

    array<Ticker::TickerItem@> getItems() {
        return items;
    }
}

class CMTickerItem : Ticker::TickerItem {
    string prefix = "\\$f47" + Icons::Circle + " Champion Medals\\$fff: ";
    string text;

    string name;
    string url;
    uint64 timestamp;

    CMTickerItem() {}

    CMTickerItem(string _name, string _url, uint64 _timestamp) {
        name = _name;
        url = _url;
        timestamp = _timestamp;

        SetItemText();
    }

    void SetItemText() {
        text = name;
    }

    string getItemText() {
        return text;
    }

    uint64 getSortTime() const {
        return timestamp;
    }

    void OnItemHovered() {}

    void OnItemClick() {
        OpenBrowserURL(url);
    }
}

class CMCampaignTickerItem : CMTickerItem {
    CMCampaignTickerItem(string _name, string _url, uint64 _timestamp) {
        name = _name;
        url = _url;
        timestamp = _timestamp;

        SetItemText();
    }

    void SetItemText() override {
        text = prefix + name;
    }
}

class CMMapTickerItem : CMTickerItem {
    uint cm_time;

    CMMapTickerItem(string _name, string _url, uint64 _timestamp, uint _cm_time) {
        name = _name;
        url = _url;
        timestamp = _timestamp;
        cm_time = _cm_time;

        SetItemText();
    }

    void SetItemText() override {
        text = prefix + name + " (" + Time::Format(cm_time) + ")";
    }
}
#else
void RegisterTicker() {}
#endif
