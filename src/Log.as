/**
 * Champion Medals
 * Copyright (c) 2023 NaNInf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

enum LogLevel {
    Trace,
    Print,
    Warn,
    Error
}

void log(const string &in message, LogLevel level) {
    if (!setting_debug_log) return;

    switch (level) {
        case LogLevel::Trace:
            trace(message);
            break;
        case LogLevel::Print:
            print(message);
            break;
        case LogLevel::Warn:
            warn(message);
            break;
        case LogLevel::Error:
            error(message);
            break;
        default:
            print(message);
            break;
    }

    if (setting_debug_log_notifications) {
        UI::ShowNotification("Champion Medals " + tostring(level), message);
    }
}
