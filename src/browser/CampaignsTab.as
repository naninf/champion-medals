/**
 * Champion Medals
 * Copyright (c) 2023 NaNInf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

class CampaignsTab : Tab {
    BrowserCampaigns@ campaigns;

    CampaignsTab(const string &in _identifier, const string &in _label, bool _closeable) {
        super(_identifier, _label, _closeable);
    }

    void Draw() override {
        if (campaigns is null) {
            UI::Text("Loading campaigns...");
            return;
        }

        if (UI::BeginTable("##Header", 3, UI::TableFlags::SizingStretchProp)) {
            UI::TableSetupColumn("##Name", UI::TableColumnFlags::WidthStretch);
            UI::TableSetupColumn("##Website", UI::TableColumnFlags::WidthFixed);
            UI::TableSetupColumn("##Refresh", UI::TableColumnFlags::WidthFixed);

            UI::TableNextRow();

            UI::TableNextColumn();
            UI::PushFont(font_manager.header);
            UI::Text(campaigns.name);
            UI::PopFont();

            UI::TableNextColumn();
            if (UI::Button(Icons::Refresh)) {
                @campaigns = null;
            }
            if (UI::IsItemHovered()) {
                UI::BeginTooltip();
                UI::Text("Refresh");
                UI::EndTooltip();
            }

            UI::TableNextColumn();
            if (UI::Button(Icons::ExternalLink)) {
                OpenBrowserURL(campaigns.url);
            }
            if (UI::IsItemHovered()) {
                UI::BeginTooltip();
                UI::Text("Open Champion Medals website");
                UI::EndTooltip();
            }

            UI::EndTable();
        }

        if (campaigns is null) return;

        UI::PushStyleVar(UI::StyleVar::CellPadding, vec2(4.0, 4.0));

        if (UI::BeginTable("##" + campaigns.name, 2)) {
            UI::TableSetupColumn("##CampaignName", UI::TableColumnFlags::WidthStretch);
            UI::TableSetupColumn("##Button", UI::TableColumnFlags::WidthFixed);

            for (uint j = 0; j < campaigns.campaigns.Length; ++j) {
                BrowserCampaign@ campaign = campaigns.campaigns[j];

                UI::TableNextRow();

                UI::TableNextColumn();
                UI::AlignTextToFramePadding();
                UI::Text(campaign.name);

                UI::TableNextColumn();

                vec2 padding = UI::GetStyleVarVec2(UI::StyleVar::FramePadding);
                vec2 text_size = Draw::MeasureString("Champion Medals");
                vec2 medal_size = vec2(16, 16);
                vec2 pos = UI::GetCursorPos();

                UI::PushStyleVar(UI::StyleVar::ButtonTextAlign, vec2(1.0, 0.0));
                if (UI::Button("Champion Medals##" + campaign.identifier,
                               vec2(3 * padding.x + UI::GetScale() * medal_size.x + text_size.x,
                               2 * padding.y + text_size.y))) {
                    if (!browser_window.SelectTab(campaign.identifier)) {
                        if (campaign.identifier == "royal") {
                            browser_window.CreateTab(RoyalCampaignsTab(campaign.identifier, campaign.name, true));
                        } else {
                            browser_window.CreateTab(CampaignTab(campaign.identifier, campaign.name, true));
                        }
                    }
                }
                UI::PopStyleVar();

                UI::SetCursorPos(pos + padding);
                medal.WindowImage(UI::GetScale() * medal_size);
            }

            UI::EndTable();
        }

        UI::PopStyleVar();
    }

    void CheckUpdate() override {
        if (campaigns is null) Update();
    }

    void Update() {
        log("Updating browser campaigns", LogLevel::Trace);
        @campaigns = null;
        browser_window.UpdateCampaignsTabs();
    }
}
