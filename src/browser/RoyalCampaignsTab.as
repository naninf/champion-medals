/**
 * Champion Medals
 * Copyright (c) 2024 NaNInf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

class RoyalCampaignsTab : CampaignsTab {
    RoyalCampaignsTab(const string &in _identifier, const string &in _label, bool _closeable) {
        super(_identifier, _label, _closeable);
    }

    void Update() override {
        log("Updating browser campaigns", LogLevel::Trace);
        @campaigns = null;

        array<BrowserCampaigns@> campaigns_categories = BrowserApi::GetCampaigns(BrowserApi::BrowserCampaignSet::Royal);
        if (campaigns_categories.Length == 1) {
            @campaigns = campaigns_categories[0];
        } else {
            log("Malformed response for Royal campaigns", LogLevel::Error);
            @campaigns = BrowserCampaigns();
        }
    }
}
