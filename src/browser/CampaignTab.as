/**
 * Champion Medals
 * Copyright (c) 2023 NaNInf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

class CampaignTab : Tab {
    BrowserCampaign@ campaign;

    CampaignTab(const string &in _identifier, const string &in _label, bool _closeable) {
        super(_identifier, _label, _closeable);
    }

    void Draw() override {
        if (campaign is null) {
            UI::Text("Loading campaign...");
            return;
        }

        bool can_play = Permissions::PlayLocalMap();

        if (UI::BeginTable("##CampaignHeader", 4, UI::TableFlags::SizingStretchProp)) {
            UI::TableSetupColumn("##Name", UI::TableColumnFlags::WidthStretch);
            UI::TableSetupColumn("##Counter", UI::TableColumnFlags::WidthFixed);
            UI::TableSetupColumn("##Website", UI::TableColumnFlags::WidthFixed);
            UI::TableSetupColumn("##Refresh", UI::TableColumnFlags::WidthFixed);

            UI::TableNextRow();

            UI::PushFont(font_manager.header);

            UI::TableNextColumn();
            UI::Text(campaign.name);

            UI::TableNextColumn();
            medal.WindowImage(UI::GetScale() * vec2(20, 20));
            UI::SameLine();
            UI::Text(campaign.GetAwardedCMCounter());

            UI::PopFont();

            UI::TableNextColumn();
            if (UI::Button(Icons::Refresh)) {
                @campaign = null;
            }
            if (UI::IsItemHovered()) {
                UI::BeginTooltip();
                UI::Text("Refresh");
                UI::EndTooltip();
            }

            UI::TableNextColumn();
            if (UI::Button(Icons::ExternalLink)) {
                OpenBrowserURL(campaign.url);
            }
            if (UI::IsItemHovered()) {
                UI::BeginTooltip();
                UI::Text("Open campaign on Champion Medals website");
                UI::EndTooltip();
            }

            UI::EndTable();
        }

        if (campaign is null) return;

        UI::PushStyleVar(UI::StyleVar::CellPadding, vec2(4.0, 4.0));

        uint num_columns = 7 + (can_play ? 1 : 0);
        int flags = UI::TableFlags::SizingStretchProp |
                    UI::TableFlags::ScrollY;

        if (UI::BeginTable("##" + campaign.identifier, num_columns, flags)) {
            UI::TableSetupColumn("Map", UI::TableColumnFlags::WidthStretch);
            UI::TableSetupColumn("##Icon1", UI::TableColumnFlags::WidthFixed);
            UI::TableSetupColumn("Time", UI::TableColumnFlags::WidthStretch);
            UI::TableSetupColumn("##Icon2", UI::TableColumnFlags::WidthFixed);
            UI::TableSetupColumn("PB", UI::TableColumnFlags::WidthStretch);
            UI::TableSetupColumn("Delta", UI::TableColumnFlags::WidthStretch);
            UI::TableSetupColumn("Count", UI::TableColumnFlags::WidthStretch);
            if (can_play) UI::TableSetupColumn("##PlayButton", UI::TableColumnFlags::WidthFixed);

            UI::TableSetupScrollFreeze(0, 1);

            UI::TableHeadersRow();

            vec2 padding = UI::GetStyleVarVec2(UI::StyleVar::FramePadding);

            for (uint i = 0; i < campaign.maps.Length; ++i) {
                BrowserMap@ map = cast<BrowserMap@>(campaign.maps[i]);

                UI::TableNextRow();

                UI::TableNextColumn();
                UI::AlignTextToFramePadding();
                UI::Text(map.name);

                UI::TableNextColumn();
                UI::SetCursorPos(UI::GetCursorPos() + vec2(0.0, padding.y));
                medal.WindowImage(UI::GetScale() * vec2(16, 16));

                UI::TableNextColumn();
                UI::Text(Time::Format(map.cm_time));

                UI::TableNextColumn();
                bool pb_available = map.pb_time != uint(-1) && map.pb_time != uint(-1) - 1;
                if (pb_available) {
                    if (map.cm_awarded()) {
                        UI::SetCursorPos(UI::GetCursorPos() + vec2(0.0, padding.y));
                        medal.WindowImage(UI::GetScale() * vec2(16, 16));
                    }

                    UI::TableNextColumn();
                    UI::Text(Time::Format(map.pb_time));

                    UI::TableNextColumn();
                    int delta = int(map.pb_time) - int(map.cm_time);
                    UI::Text(UIUtils::DeltaString(delta));
                } else {
                    UI::TableNextColumn();
                    UI::TableNextColumn();
                }

                UI::TableNextColumn();
                UI::Text(tostring(map.count));

                if (can_play) {
                    UI::TableNextColumn();
                    if (UI::Button(Icons::Play + "##" + map.uid)) {
                        startnew(CoroutineFunc(map.PlayCoroutine));
                    }
                    if (UI::IsItemHovered()) {
                        UI::BeginTooltip();
                        UI::Text("Play map");
                        UI::EndTooltip();
                    }
                }
            }

            UI::EndTable();
        }

        UI::PopStyleVar();
    }

    void CheckUpdate() override {
        if (campaign is null) Update();
    }

    void Update() {
        log("Updating browser campaign, " + identifier, LogLevel::Trace);
        @campaign = null;
        @campaign = BrowserApi::GetCampaign(identifier);
    }
}
