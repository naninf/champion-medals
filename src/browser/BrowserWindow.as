/**
 * Champion Medals
 * Copyright (c) 2023 NaNInf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

class BrowserWindow {
    vec4 base_color = vec4(0.609375, 0, 0.149020, 1.0);

    array<Tab@> tabs;
    uint active_tab_index = 0;

    BrowserWindow() {}

    void RenderMenu() {
        if (UI::MenuItem("\\$f47" + Icons::Circle + "\\$fff Champion Medals", "", setting_browser_window_enable)) {
            setting_browser_window_enable = !setting_browser_window_enable;
        }
    }

    bool WindowVisible() {
        return setting_browser_window_enable;
    }

    void Draw() {
        if (!WindowVisible()) return;

        UI::SetNextWindowSize(720, 640, UI::Cond::FirstUseEver);

        UI::Begin("\\$f47" + Icons::Circle + "\\$fff Champion Medals", setting_browser_window_enable);

        UI::PushStyleColor(UI::Col::Button, base_color);
        UI::PushStyleColor(UI::Col::ButtonHovered, base_color * vec4(1.1, 1.1, 1.1, 1.0));
        UI::PushStyleColor(UI::Col::ButtonActive, base_color * vec4(0.9, 0.9, 0.9, 1.0));
        UI::PushStyleColor(UI::Col::Tab, base_color * vec4(0.7, 0.7, 0.7, 1.0));
        UI::PushStyleColor(UI::Col::TabHovered, base_color * vec4(1.1, 1.1, 1.1, 1.0));
        UI::PushStyleColor(UI::Col::TabActive, base_color);
        UI::PushStyleColor(UI::Col::HeaderHovered, vec4(0.0));
        UI::PushStyleColor(UI::Col::HeaderActive, vec4(0.0));

        if (tabs.Length == 0) {
            UI::Text("Loading campaigns...");
        } else {
            UI::BeginTabBar("##BrowserTab", UI::TabBarFlags::AutoSelectNewTabs);

            uint close_tab_index = uint(-1);

            for (uint i = 0; i < tabs.Length; ++i) {
                Tab@ tab = tabs[i];
                bool close;
                if (tab.BeginTabItem(close)) {
                    active_tab_index = i;
                    UI::BeginChild("##TabIndex" + tostring(i));
                    tab.Draw();
                    UI::EndChild();
                    UI::EndTabItem();
                }

                if (close) {
                    close_tab_index = i;
                }
            }

            if (close_tab_index != uint(-1)) tabs.RemoveAt(close_tab_index);

            UI::EndTabBar();
        }

        UI::PopStyleColor(8);

        UI::End();
    }

    void CheckUpdate() {
        if (!WindowVisible()) return;

        if (tabs.Length == 0) UpdateCampaignsTabs(true);

        if (active_tab_index < tabs.Length) tabs[active_tab_index].CheckUpdate();
    }

    bool SelectTab(const string &in identifier) {
        for (uint i = 0; i < tabs.Length; ++i) {
            Tab@ tab = tabs[i];
            if (tab.identifier == identifier) {
                tab.Select();
                return true;
            }
        }
        return false;
    }

    void CreateTab(Tab@ tab) {
        tabs.InsertLast(tab);
    }

    void UpdateCampaignsTabs(bool initial = false) {
        array<BrowserCampaigns@> campaigns_categories = BrowserApi::GetCampaigns(BrowserApi::BrowserCampaignSet::Base);

        for (int i = tabs.Length - 1; i >= 0; --i) if (!tabs[i].closeable) tabs.RemoveAt(i);

        for (uint i = 0; i < campaigns_categories.Length; ++i) {
            BrowserCampaigns@ campaigns = campaigns_categories[i];
            CampaignsTab@ tab = CampaignsTab(campaigns.identifier, campaigns.name, false);
            @tab.campaigns = campaigns;
            CreateTab(tab);
        }

        if (initial && campaigns_categories.Length > 0) SelectTab(campaigns_categories[0].identifier);
    }
}
