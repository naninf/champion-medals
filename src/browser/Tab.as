/**
 * Champion Medals
 * Copyright (c) 2023 NaNInf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

class Tab {
    string identifier;
    string label;
    bool closeable;

    bool selected_flag;

    string get_id() const property {
        return label + "##" + identifier;
    }

    Tab(const string &in _identifier, const string &in _label, bool _closeable) {
        identifier = _identifier;
        label = _label;
        closeable = _closeable;
    }

    bool BeginTabItem(bool &out close) {
        int flags = 0;
        if (selected_flag) {
            flags |= UI::TabItemFlags::SetSelected;
            selected_flag = false;
        }

        if (closeable) {
            bool not_close;
            bool opened = UI::BeginTabItem(id, not_close, flags);
            close = !not_close;
            return opened;
        } else {
            close = false;
            return UI::BeginTabItem(id, flags);
        }
    }

    void Select() {
        selected_flag = true;
    }

    void Draw() {}

    void CheckUpdate() {}
}
