/**
 * Champion Medals
 * Copyright (c) 2023 NaNInf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

enum Align {
    Left = 1 << 0,
    Center = 1 << 1,
    Right = 1 << 2,
    Top = 1 << 3,
    Middle = 1 << 4,
    Bottom = 1 << 5
};

class Medal {
    UI::Texture@ ui_medal;
    nvg::Texture@ nvg_medal;

    Medal() {
        @ui_medal = UI::LoadTexture("assets/champion_medal_32.png");
        @nvg_medal = nvg::LoadTexture("assets/champion_medal_256.png", nvg::TextureFlags::GenerateMipmaps);
    }

    void WindowImage(vec2 size) {
        UI::Image(ui_medal, size);
    }

    void NvgImage(vec2 size, vec2 coords, uint align = Align::Left | Align::Top,
                       float scale = 1.0, float alpha = 1.0) {
        vec2 coords_offset;
        if (align & Align::Left != 0) coords_offset.x = -size.x * (scale - 1) / 2;
        if (align & Align::Center != 0) coords_offset.x = -size.x * scale / 2;
        if (align & Align::Right != 0) coords_offset.x = -size.x * (scale + 1) / 2;
        if (align & Align::Top != 0) coords_offset.y = -size.y * (scale - 1) / 2;
        if (align & Align::Middle != 0) coords_offset.y = -size.y * scale / 2;
        if (align & Align::Bottom != 0) coords_offset.y = -size.y * (scale + 1) / 2;

        nvg::BeginPath();
        nvg::Rect(coords + coords_offset, size * scale);
        nvg::FillPaint(nvg::TexturePattern(coords + coords_offset, size * scale, 0, nvg_medal, alpha));
        nvg::Fill();
        nvg::ClosePath();
    }
}
