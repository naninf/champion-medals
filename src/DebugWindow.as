/**
 * Champion Medals
 * Copyright (c) 2023 NaNInf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

class DebugWindow {
    DebugWindow() {}

    void Draw() {
        if (!setting_debug_window_enable) return;

        int window_flags = UI::WindowFlags::NoCollapse |
                           UI::WindowFlags::NoDocking;

        UI::Begin(Icons::Bug + " Champion Medals Debug", setting_debug_window_enable, window_flags);

        UI::BeginTabBar("CMDebugTab");

        if (UI::BeginTabItem("Layers")) {
            DrawLayers();
            UI::EndTabItem();
        }

        if (UI::BeginTabItem("Map")) {
            DrawMap();
            UI::EndTabItem();
        }

        UI::EndTabBar();

        UI::End();
    }

    private void DrawLayers() {
        if (UI::BeginTable("LayersTable", 6, UI::TableFlags::SizingFixedFit)) {
            UI::TableNextRow();
            UI::TableNextColumn();
            Text("Layer");

            UI::TableNextColumn();
            Text("Index");

            UI::TableNextColumn();
            Text("Not null");

            UI::TableNextColumn();
            Text("IsVisible");

            UI::TableNextColumn();
            Text("ShouldDraw");

            UI::TableNextColumn();
            Text("Identifier");

            for (uint i = 0; i < layer_manager.layers.Length; ++i) {
                Layer@ layer = layer_manager.layers[i];
                string label = tostring(LayerIndex(i));

                UI::TableNextRow();
                UI::TableNextColumn();
                Text(label);

                UI::TableNextColumn();
                Text(tostring(layer.ui_layer_index));

                UI::TableNextColumn();
                Checkbox("##" + label + "NotNull", !layer.UILayerIsNull());
                if (layer.UILayerIsNull()) continue;

                UI::TableNextColumn();
                Checkbox("##" + label + "IsVisible", layer.IsVisible());

                UI::TableNextColumn();
                Checkbox("##" + label + "ShouldDraw", layer.ShouldDraw());

                UI::TableNextColumn();
                Copyable(layer.GetIdentifier());
            }

            // layers not in Layers.get_layers()
            PrestigeLayer@ prestige_layer = layer_manager.layers.prestige_layer;

            UI::TableNextRow();
            UI::TableNextColumn();
            Text("PrestigeLayer");

            UI::TableNextColumn();
            Text(tostring(prestige_layer.ui_layer_index));

            UI::TableNextColumn();
            Checkbox("##PrestigeLayerNotNull", !prestige_layer.UILayerIsNull());
            if (!prestige_layer.UILayerIsNull()) {
                UI::TableNextColumn();
                Checkbox("##PrestigeLayerIsVisible", prestige_layer.IsVisible());
            }

            ScoresTableLayer@ scores_table_layer = layer_manager.layers.scores_table_layer;

            UI::TableNextRow();
            UI::TableNextColumn();
            Text("ScoresTableLayer");

            UI::TableNextColumn();
            Text(tostring(scores_table_layer.ui_layer_index));

            UI::TableNextColumn();
            Checkbox("##ScoresTableLayerNotNull", !scores_table_layer.UILayerIsNull());
            if (!scores_table_layer.UILayerIsNull()) {
                UI::TableNextColumn();
                Checkbox("##ScoresTableLayerIsVisible", scores_table_layer.IsVisible());
            }

            CloseDialogLayer@ close_dialog_layer = layer_manager.layers.close_dialog_layer;

            UI::TableNextRow();
            UI::TableNextColumn();
            Text("CloseDialogLayer");

            UI::TableNextColumn();
            UI::TableNextColumn();
            Checkbox("##CloseDialogLayerNotNull", !close_dialog_layer.OverlayIsNull());
            if (!close_dialog_layer.OverlayIsNull()) {
                UI::TableNextColumn();
                Checkbox("##CloseDialogLayerIsVisible", close_dialog_layer.IsVisible());
            }

            UI::EndTable();
        }
    }

    private void DrawMap() {
        if (UI::BeginTable("LayersTable", 2, UI::TableFlags::SizingFixedFit)) {
            UI::TableNextRow();
            UI::TableNextColumn();
            Text("Not null");

            UI::TableNextColumn();
            Checkbox("##MapNotNull", playground_map !is null);

            if (playground_map is null) {
                UI::EndTable();
                return;
            }

            UI::TableNextRow();
            UI::TableNextColumn();
            Text("Has CM");

            UI::TableNextColumn();
            Checkbox("##MapHasCM", playground_map.has_cm);

            UI::TableNextRow();
            UI::TableNextColumn();
            Text("UID");

            UI::TableNextColumn();
            Copyable(playground_map.uid);

            UI::TableNextRow();
            UI::TableNextColumn();
            Text("CM time");

            UI::TableNextColumn();
            Text(Time::Format(playground_map.cm_time));

            UI::TableNextRow();
            UI::TableNextColumn();
            Text("PB time");

            UI::TableNextColumn();
            Text(Time::Format(playground_map.pb_time));

            UI::EndTable();
        }
    }

    private void Text(const string text) {
        UI::AlignTextToFramePadding();
        UI::Text(text);
    }

    private void Copyable(const string text) {
        UI::AlignTextToFramePadding();
        if (UI::Selectable(text, false)) {
            UI::ShowNotification("Copied " + text);
            IO::SetClipboard(text);
        }
    }

    private void Checkbox(const string id, const bool checked) {
        UI::BeginDisabled();
        UI::Checkbox(id, checked);
        UI::EndDisabled();
    }
}
