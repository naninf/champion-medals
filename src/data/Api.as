/**
 * Champion Medals
 * Copyright (c) 2023 NaNInf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Api {
    // relative to base url
    const string cm_campaign_endpoint = "/api/campaign/";  // + campaign identifier
    const string cm_map_endpoint = "/api/map/";  // + map UID
    const string cm_ticker_endpoint = "/api/ticker";

    const string tm_map_endpoint = "https://live-services.trackmania.nadeo.live/api/token/map/";  // + map UID
    const string tm_maps_endpoint = "https://live-services.trackmania.nadeo.live/api/token/leaderboard/group/map";

    Campaign@ GetCampaignChampionMedals(const string &in identifier) {
        log("Campaign: " + identifier, LogLevel::Trace);

        Net::HttpRequest@ req = Net::HttpRequest();
        req.Url = setting_api_base_url + cm_campaign_endpoint + Net::UrlEncode(identifier);

        req.Start();
        while (!req.Finished()) yield();

        if (req.ResponseCode() == 204) {
            log("No champion medals", LogLevel::Trace);
            return Campaign();
        } else if (req.ResponseCode() != 200) {
            log("Champion medal request returned response code " + req.ResponseCode(), LogLevel::Error);
            log("Response body:", LogLevel::Error);
            log(req.Body, LogLevel::Error);
            return Campaign();
        }

        Campaign@ campaign = Campaign();
        campaign.has_cms = true;

        Json::Value res = Json::Parse(req.String());

        for (uint i = 0; i < res["maps"].Length; ++i) {
            Json::Value res_map = res["maps"][i];
            Map@ map = Map(res_map["tm_uid"], res_map["cm_time"], res_map["index"]);
            campaign.maps.InsertLast(map);

            log("Champion medal: " + map.uid + " (" + map.cm_time + ")", LogLevel::Trace);
        }

        return campaign;
    }

    void AddCampaignPBs(CampaignBase@ campaign) {
        Json::Value req_body = Json::Object();
        req_body["maps"] = Json::Array();

        for (uint i = 0; i < campaign.maps.Length; ++i) {
            MapBase@ map = campaign.maps[i];
            Json::Value req_map = Json::Object();
            req_map["groupUid"] = "Personal_Best";
            req_map["mapUid"] = map.uid;
            req_body["maps"].Add(req_map);
        }

        Net::HttpRequest@ req = NadeoServices::Post("NadeoLiveServices", tm_maps_endpoint, Json::Write(req_body));
        req.Start();
        while (!req.Finished()) yield();

        if (req.ResponseCode() != 200) {
            log("TM API request returned response code " + req.ResponseCode(), LogLevel::Error);
            log("Response body:", LogLevel::Error);
            log(req.Body, LogLevel::Error);
            return;
        }

        Json::Value res = Json::Parse(req.String());

        // this result is in the same order as the campaign maps, and contains
        // either the full set or a subset
        uint ci = 0;  // index for traversing through campaign maps
        for (uint i = 0; i < res.Length; ++i) {
            Json::Value res_map = res[i];
            string uid = res_map["mapUid"];
            uint pb_time = res_map["score"];
            while (campaign.maps[ci].uid != uid) {
                if (++ci >= campaign.maps.Length) ci = 0;  // this should never happen, but just to be safe
            }
            campaign.maps[ci].pb_time = pb_time;

            log("PB: " + uid + " (" + pb_time + ")", LogLevel::Trace);
        }
    }

    Campaign@ GetCampaign(const string &in identifier) {
        Campaign@ campaign = GetCampaignChampionMedals(identifier);
        AddCampaignPBs(campaign);
        return campaign;
    }

    Map@ GetMapChampionMedal(const string &in uid) {
        log("Map: " + uid, LogLevel::Trace);

        Net::HttpRequest@ req = Net::HttpRequest();
        req.Url = setting_api_base_url + cm_map_endpoint + Net::UrlEncode(uid);

        req.Start();
        while (!req.Finished()) yield();

        if (req.ResponseCode() == 204) {
            log("No champion medal", LogLevel::Trace);
            return Map(uid);
        } else if (req.ResponseCode() != 200) {
            log("Champion medal request returned response code " + req.ResponseCode(), LogLevel::Error);
            log("Response body:", LogLevel::Error);
            log(req.Body, LogLevel::Error);
            return Map(uid);
        }

        Json::Value res_map = Json::Parse(req.String());

        Map@ map = Map(res_map["tm_uid"], res_map["cm_time"]);

        log("Map: " + map.uid + " (" + map.cm_time + ")", LogLevel::Trace);

        return map;
    }

    string GetMapUrl(const string &in map_uid) {
        Net::HttpRequest@ req = NadeoServices::Get("NadeoLiveServices", tm_map_endpoint + map_uid);
        req.Start();
        while (!req.Finished()) yield();

        if (req.ResponseCode() != 200) {
            log("TM API request returned response code " + req.ResponseCode(), LogLevel::Error);
            log("Response body:", LogLevel::Error);
            log(req.Body, LogLevel::Error);
            return "";
        }

        Json::Value res = Json::Parse(req.String());
        string url = res["downloadUrl"];
        return url;
    }

#if DEPENDENCY_TICKER
    array<Ticker::TickerItem@> GetTicker() {
        Net::HttpRequest@ req = Net::HttpRequest();
        req.Url = setting_api_base_url + cm_ticker_endpoint;

        req.Start();
        while (!req.Finished()) yield();

        if (req.ResponseCode() != 200) {
            log("Champion medal request returned response code " + req.ResponseCode(), LogLevel::Error);
            log("Response body:", LogLevel::Error);
            log(req.Body, LogLevel::Error);
            return array<Ticker::TickerItem@>();
        }

        Json::Value res = Json::Parse(req.String());

        array<Ticker::TickerItem@> items;

        for (uint i = 0; i < res["items"].Length; ++i) {
            Json::Value res_item = res["items"][i];

            string type = res_item["type"];
            if (type == "campaign") {
                CMCampaignTickerItem@ item = CMCampaignTickerItem(res_item["name"],
                                                                  res_item["url"],
                                                                  res_item["timestamp"]);
                items.InsertLast(item);

                log("Ticker item: " + item.name, LogLevel::Trace);
            } else if (type == "map") {
                CMMapTickerItem@ item = CMMapTickerItem(res_item["name"],
                                                        res_item["url"],
                                                        res_item["timestamp"],
                                                        res_item["cm_time"]);
                items.InsertLast(item);

                log("Ticker item: " + item.name, LogLevel::Trace);
            } else {
                log("Ticker item type " + type + " not implemented.", LogLevel::Warn);
            }
        }

        return items;
    }
#endif
}
