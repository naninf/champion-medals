/**
 * Champion Medals
 * Copyright (c) 2023 NaNInf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

class BrowserCampaign : CampaignBase {
    string name = "";
    string url = "";

    BrowserCampaign() {}

    BrowserCampaign(const string &in _name, const string &in _identifier, const string &in _url) {
        name = _name;
        identifier = _identifier;
        url = _url;
    }

    string GetAwardedCMCounter() {
        uint num_awarded = 0;
        uint num_maps = maps.Length;

        for (uint i = 0; i < maps.Length; ++i) {
            if (maps[i].cm_awarded()) num_awarded++;
        }

        return tostring(num_awarded) + "/" + tostring(num_maps);
    }
}
