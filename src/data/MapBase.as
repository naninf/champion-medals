/**
 * Champion Medals
 * Copyright (c) 2023 NaNInf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

class MapBase {
    bool has_cm;
    string uid;
    uint cm_time = 0;
    uint pb_time = uint(-1) - 1;

    bool cm_awarded() {
        return cm_awarded(pb_time);
    }

    bool cm_awarded(uint time) {
        return has_cm && time <= cm_time;
    }
}
