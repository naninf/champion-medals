/**
 * Champion Medals
 * Copyright (c) 2024 NaNInf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace BrowserApi {
    // relative to base url
    const string cm_browser_campaigns_endpoint = "/api/browser";
    const string cm_browser_campaign_endpoint = "/api/browser/";  // + campaign identifier

    array<BrowserCampaigns@> GetCampaigns(BrowserCampaignSet campaign_set = BrowserCampaignSet::Base) {
        log("Getting campaigns", LogLevel::Trace);

        array<BrowserCampaigns@> campaigns_categories;

        Net::HttpRequest@ req = Net::HttpRequest();

        switch (campaign_set) {
            case BrowserCampaignSet::Royal:
                req.Url = req.Url = setting_api_base_url + cm_browser_campaigns_endpoint + "/royal";
                break;
            case BrowserCampaignSet::Base:
            default:
                req.Url = setting_api_base_url + cm_browser_campaigns_endpoint;
                break;
        }

        req.Start();
        while (!req.Finished()) yield();

        if (req.ResponseCode() == 204) {
            log("No champion medals", LogLevel::Trace);
            return campaigns_categories;
        } else if (req.ResponseCode() != 200) {
            log("Champion medal request returned response code " + req.ResponseCode(), LogLevel::Error);
            log("Response body:", LogLevel::Error);
            log(req.Body, LogLevel::Error);
            return campaigns_categories;
        }

        Json::Value res = Json::Parse(req.String());

        for (uint i = 0; i < res["categories"].Length; ++i) {
            BrowserCampaigns@ campaigns = BrowserCampaigns();

            Json::Value res_category = res["categories"][i];
            campaigns.identifier = res_category["identifier"];
            campaigns.name = res_category["name"];
            campaigns.url = res_category["url"];

            for (uint j = 0; j < res_category["campaigns"].Length; ++j) {
                Json::Value res_campaign = res_category["campaigns"][j];
                BrowserCampaign@ campaign = BrowserCampaign(res_campaign["name"], res_campaign["identifier"], res_campaign["url"]);
                campaigns.campaigns.InsertLast(campaign);
            }

            if (campaigns.campaigns.Length > 0) {
                campaigns_categories.InsertLast(campaigns);
            }
        }

        return campaigns_categories;
    }

    BrowserCampaign@ GetCampaignChampionMedals(const string &in identifier) {
        log("Campaign: " + identifier, LogLevel::Trace);

        Net::HttpRequest@ req = Net::HttpRequest();
        req.Url = setting_api_base_url + cm_browser_campaign_endpoint + Net::UrlEncode(identifier);

        req.Start();
        while (!req.Finished()) yield();

        if (req.ResponseCode() == 204) {
            log("No champion medals", LogLevel::Trace);
            return BrowserCampaign();
        } else if (req.ResponseCode() != 200) {
            log("Champion medal request returned response code " + req.ResponseCode(), LogLevel::Error);
            log("Response body:", LogLevel::Error);
            log(req.Body, LogLevel::Error);
            return BrowserCampaign();
        }

        Json::Value res = Json::Parse(req.String());

        BrowserCampaign@ campaign = BrowserCampaign(res["name"], res["identifier"], res["url"]);

        for (uint i = 0; i < res["maps"].Length; ++i) {
            Json::Value res_map = res["maps"][i];
            BrowserMap@ map = BrowserMap(res_map["name"], res_map["tm_uid"], res_map["cm_time"], res_map["count"]);
            campaign.maps.InsertLast(map);

            log("Champion medal: " + map.uid + " (" + map.cm_time + ")", LogLevel::Trace);
        }

        return campaign;
    }

    BrowserCampaign@ GetCampaign(const string &in identifier) {
        BrowserCampaign@ campaign = GetCampaignChampionMedals(identifier);
        Api::AddCampaignPBs(campaign);
        return campaign;
    }

    enum BrowserCampaignSet {
        Base,
        Royal
    }
}
