/**
 * Champion Medals
 * Copyright (c) 2023 NaNInf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

class Map : MapBase {
    uint index;

    Map(string _uid) {
        has_cm = false;
        uid = _uid;
    }

    Map(string _uid, uint _cm_time) {
        has_cm = true;
        uid = _uid;
        cm_time = _cm_time;
    }

    Map(string _uid, uint _cm_time, uint _index) {
        has_cm = true;
        uid = _uid;
        cm_time = _cm_time;
        index = _index;
    }
}
