/**
 * Champion Medals
 * Copyright (c) 2024 NaNInf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

class LayerManager {
    Layers@ layers;

    bool menu_layers_set = false;
    bool playground_layers_set = false;

    LayerManager() {
        @layers = Layers();
    }

    void CheckUpdate() {
        for (uint i = 0; i < layers.Length; ++i) layers[i].CheckUpdate();
    }

    void CheckUpdateUILayers() {
        CTrackMania@ app = cast<CTrackMania@>(GetApp());

        bool menu_visible = app.MenuManager.MenuCustom_CurrentManiaApp !is null &&
            app.Network.ClientManiaAppPlayground is null &&
            app.Editor is null;

        if (!menu_layers_set && menu_visible) SetMenuLayers();
        if (menu_layers_set && !menu_visible) ClearMenuLayers();

        if (!playground_layers_set && app.Network.ClientManiaAppPlayground !is null) SetPlaygroundLayers();
        if (playground_layers_set && app.Network.ClientManiaAppPlayground is null) ClearPlaygroundLayers();

        SetCloseDialogLayer();
    }

    void Draw() {
        for (uint i = 0; i < layers.Length; ++i) layers[i].Draw();
    }

    private void SetMenuLayers() {
        CTrackMania@ app = cast<CTrackMania@>(GetApp());

        auto ui_layers = app.MenuManager.MenuCustom_CurrentManiaApp.UILayers;

        uint layers_set = 0;

        for (uint index = 0; index < ui_layers.Length; ++index) {
            CGameUILayer@ ui_layer = ui_layers[index];

            string header = ui_layer.ManialinkPageUtf8.SubStr(0, 100);

            if (header.IndexOf("\"Page_CampaignDisplay\"") != -1) {
                layers.regular_campaign_layer.SetUILayerIndex(index);
                ++layers_set;
            }

            if (header.IndexOf("\"Page_RoomMapListDisplay\"") != -1) {
                layers.regular_campaign_live_layer.SetUILayerIndex(index);
                ++layers_set;
            }

            if (header.IndexOf("\"Page_RoomCampaignDisplay\"") != -1) {
                layers.regular_official_campaign_live_layer.SetUILayerIndex(index);
                ++layers_set;
            }

            if (header.IndexOf("\"Page_MonthlyCampaignDisplay\"") != -1) {
                layers.monthly_campaign_layer.SetUILayerIndex(index);
                ++layers_set;
            }
        }

        if (layers_set > 0) menu_layers_set = true;
    }

    private void ClearMenuLayers() {
        layers.regular_campaign_layer.ClearUILayerIndex();
        layers.regular_campaign_live_layer.ClearUILayerIndex();
        layers.regular_official_campaign_live_layer.ClearUILayerIndex();
        layers.monthly_campaign_layer.ClearUILayerIndex();

        menu_layers_set = false;
    }

    private void SetPlaygroundLayers() {
        CTrackMania@ app = cast<CTrackMania@>(GetApp());

        // solo and live modes have different playground layers
        bool solo = cast<CGameCtnNetServerInfo>(app.Network.ServerInfo).ServerLogin == "";

        auto ui_layers = app.Network.ClientManiaAppPlayground.UILayers;

        uint layers_set = 0;

        for (uint index = 0; index < ui_layers.Length; ++index) {
            CGameUILayer@ ui_layer = ui_layers[index];

            string header = ui_layer.ManialinkPageUtf8.SubStr(0, 100);

            if (solo) {
                if (header.IndexOf("\"UIModule_Campaign_StartRaceMenu\"") != -1) {
                    layers.start_menu_layer.SetUILayerIndex(index);
                    ++layers_set;
                }

                if (header.IndexOf("\"UIModule_Campaign_PauseMenu\"") != -1 ||
                    header.IndexOf("\"UIModule_PlayMap_PauseMenu\"") != -1) {
                    layers.pause_menu_layer.SetUILayerIndex(index);
                    ++layers_set;
                }

                if (header.IndexOf("\"UIModule_Campaign_EndRaceMenu\"") != -1) {
                    layers.end_menu_layer.SetUILayerIndex(index);
                    ++layers_set;
                }

                if (header.IndexOf("\"UIModule_PlayMap_EndRaceMenu\"") != -1) {
                    layers.local_end_menu_layer.SetUILayerIndex(index);
                    ++layers_set;
                }
            } else {
                if (header.IndexOf("\"UIModule_Online_PauseMenu\"") != -1) {
                    layers.live_pause_menu_layer.SetUILayerIndex(index);
                    ++layers_set;
                }
            }

            if (header.IndexOf("\"UIModule_Race_Record\"") != -1) {
                layers.record_layer.SetUILayerIndex(index);
                ++layers_set;
            }

            if (header.IndexOf("\"UIModule_Race_PrestigeEarned\"") != -1) {
                layers.prestige_layer.SetUILayerIndex(index);
                ++layers_set;
            }

            if (header.IndexOf("\"UIModule_Race_ScoresTable\"") != -1) {
                layers.scores_table_layer.SetUILayerIndex(index);
                ++layers_set;
            }
        }

        if (layers_set > 0) playground_layers_set = true;
    }

    private void ClearPlaygroundLayers() {
        layers.start_menu_layer.ClearUILayerIndex();
        layers.pause_menu_layer.ClearUILayerIndex();
        layers.end_menu_layer.ClearUILayerIndex();
        layers.local_end_menu_layer.ClearUILayerIndex();
        layers.record_layer.ClearUILayerIndex();
        layers.live_pause_menu_layer.ClearUILayerIndex();
        layers.live_new_pb_layer.ClearUILayerIndex();
        layers.prestige_layer.ClearUILayerIndex();
        layers.scores_table_layer.ClearUILayerIndex();

        playground_layers_set = false;
    }

    private void SetCloseDialogLayer() {
        layers.close_dialog_layer.SetOverlay(null);

        CHmsViewport@ viewport = GetApp().Viewport;

        if (viewport.Overlays.Length == 0) return;

        // the close dialog is always the last overlay
        CHmsZoneOverlay@ overlay = viewport.Overlays[viewport.Overlays.Length - 1];
        if (overlay.m_CorpusVisibles.Length == 0) return;

        CControlContainer@ scene_mobil = cast<CControlContainer@>(overlay.m_CorpusVisibles[0].Item.SceneMobil);
        if (scene_mobil is null) return;
        if (scene_mobil.IdName != "FrameConfirmQuit") return;

        layers.close_dialog_layer.SetOverlay(overlay);
    }
}
